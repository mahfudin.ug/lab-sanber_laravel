console.log(`3. Destructuring`)
// const newObject = {
//   firstName: "Harry",
//   lastName: "Potter Holt",
//   destination: "Hogwarts React Conf",
//   occupation: "Deve-wizard Avocado",
//   spell: "Vimulus Renderus!!!"
// }
// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const destination = newObject.destination;
// const occupation = newObject.occupation;
// // Driver code
// console.log(firstName, lastName, destination, occupation)

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}
const {firstName, lastName, destination, occupation} = newObject;
// Driver code
console.log(firstName, lastName, destination, occupation)