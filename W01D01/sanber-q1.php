<?php
        //Tested on PHP 7.2.33
trait Hewan {
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;
    
    abstract public function getInfoHewan();
    
    public function atraksi() {
        echo "{$this->nama} sedang {$this->keahlian}".PHP_EOL;
    }
}

trait Fight {
    public $attackPower;
    public $defencePower;
    
    public function serang ($lawan){
        echo "{$this->nama} sedang menyerang {$lawan->nama}".PHP_EOL;
        $lawan->diserang($this);
    }
    
    public function diserang ($lawan){
        echo "{$this->nama} sedang diserang".PHP_EOL;
        $this->darah = $this->darah - $lawan->attackPower / $this->defencePower;
    }
    
}

class Elang {
    use Hewan, Fight;
    
    function __construct($nama, $darah, $jumlahKaki, $keahlian, $attackP, $defenceP) {
        $this->nama = $nama;
        $this->darah = $darah;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackP;
        $this->defencePower = $defenceP;
    }
    
    public function getInfoHewan() {
        echo "================================".PHP_EOL;
        echo "Nama : {$this->nama}".PHP_EOL;
        echo "HP : {$this->darah}".PHP_EOL;
        echo "Kaki : {$this->jumlahKaki}".PHP_EOL;
        echo "Keahlian : {$this->keahlian}".PHP_EOL;
        echo "Attack : {$this->attackPower}".PHP_EOL;
        echo "Defense : {$this->defencePower}".PHP_EOL;
        echo "Type : ". get_class() .PHP_EOL;
        echo "================================".PHP_EOL;
    }
}

class Harimau {
    use Hewan, Fight;
    
    function __construct($nama, $darah, $jumlahKaki, $keahlian, $attackP, $defenceP) {
        $this->nama = $nama;
        $this->darah = $darah;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackP;
        $this->defencePower = $defenceP;
    }
    
    public function getInfoHewan() {
        echo "================================".PHP_EOL;
        echo "Nama : {$this->nama}".PHP_EOL;
        echo "HP : {$this->darah}".PHP_EOL;
        echo "Kaki : {$this->jumlahKaki}".PHP_EOL;
        echo "Keahlian : {$this->keahlian}".PHP_EOL;
        echo "Attack : {$this->attackPower}".PHP_EOL;
        echo "Defense : {$this->defencePower}".PHP_EOL;
        echo "Type : ". get_class() .PHP_EOL;
        echo "================================".PHP_EOL;
    }
}

// IMPLEMENTATION
$anakElang = new Elang('elang_3', 100, 2, 'terbang tinggi', 10, 5);
$anakHarimau = new Harimau('harimau_1', 150, 4, 'lari cepat', 7, 8);

$anakElang->getInfoHewan();
$anakHarimau->getInfoHewan();

$anakElang->atraksi();
$anakHarimau->atraksi();
echo PHP_EOL.PHP_EOL;
$anakElang->serang($anakHarimau);
echo PHP_EOL.PHP_EOL;

$anakElang->getInfoHewan();
$anakHarimau->getInfoHewan();

